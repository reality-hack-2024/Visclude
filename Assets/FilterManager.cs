using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FilterManager : MonoBehaviour
{
    public GameObject onSwitch;
    public GameObject offSwitch;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnFilterOff(){
        Debug.Log("----on filter off");
        onSwitch.SetActive(false);
        offSwitch.SetActive(true);
    }
    public void OnFilterOn(){
        Debug.Log("----on filter on");
       onSwitch.SetActive(true);
        offSwitch.SetActive(false);
    }
}
