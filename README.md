# Visclude

<img width="600" alt="logo" src="https://github.com/petersun825/Visclude/assets/56364747/e49a58c0-7167-4326-a6d2-2ef56453bdfb">

A Developer Tool for Visual Accessibility Design in XR
2024 MIT Reality Hack

**Team members**:
Ardak Mukanova, Peter Sun, Prashasti Kapadia, Rajashekar Vennanelli, Yixuan Liu

## Setup

### Hardware Required

- Meta Quest 3

### Software Dependencies

- Unity 2022.3.18f1

## Scene description

*RoomScene* default settings:
   - Using Meta Presence Platform Passthrough
     - Edge rendering
     - Contrast/brightness/greyscale changes
   - Using Presence Platform room model
     - Furniture detection
   - Visual blocks for the user
     - glaucoma
     - diebetic retinopathy


## Shoutouts
Chris McNally, Lucas Martinic & Gregory
